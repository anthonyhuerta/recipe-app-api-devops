resource "aws_s3_bucket" "app_public_files" {
  bucket        = "${local.prefix}-huerta-files"
  acl           = "public-read"
  force_destroy = true
}
